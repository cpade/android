package fr.enssat.clientsocketserver.socket.server

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import fr.enssat.clientsocketserver.NetworkUtils
import fr.enssat.clientsocketserver.R
import fr.enssat.clientsocketserver.databinding.ActivityClientBinding
import fr.enssat.clientsocketserver.multicast.MessageAdapter
import fr.enssat.clientsocketserver.multicast.MultiCastViewModel
import fr.enssat.clientsocketserver.multicast.MulticastViewModelFactory

class ServerActivity : AppCompatActivity() {
    lateinit var binding: ActivityClientBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val multicastModel = ViewModelProviders.of(this, MulticastViewModelFactory(this)).get(MultiCastViewModel::class.java)

        val model = ViewModelProviders.of(this,
            ServerViewModelFactory(this)
        ).get(ServerViewModel::class.java)

        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_main
        )

        binding.serverTitle.text = "server listening on " + NetworkUtils.getIpAddress(
            this
        ) + ": ${ServerSocket.PORT}"

        val adapter = MessageAdapter {}
        binding.messageList.adapter = adapter

        model.messages.observe(this, Observer { list ->
            adapter.list = list
        })


        /************* multicast ***************/

        /*val multicastModel = ViewModelProviders.of(this,
            MulticastViewModelFactory(this)
        ).get(
            MultiCastViewModel::class.java)

        binding.sendResults.setOnClickListener {
            val iterator = model.messages.value?.iterator()

            iterator?.forEach {
                multicastModel.send(it)
            }
        }

        val multicastAdapter = MessageAdapter {}
        binding.messageList.adapter = multicastAdapter

        multicastModel.messages.observe(this, Observer { list ->
            multicastAdapter.list = list
        })*/
    }
 }
