package fr.enssat.clientsocketserver.socket.client

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import fr.enssat.clientsocketserver.R
import fr.enssat.clientsocketserver.socket.server.ServerSocket
import fr.enssat.clientsocketserver.databinding.ActivityClientBinding
import fr.enssat.clientsocketserver.multicast.MessageAdapter
import fr.enssat.clientsocketserver.multicast.MultiCastViewModel
import fr.enssat.clientsocketserver.multicast.MulticastViewModelFactory

class ClientActivity : AppCompatActivity() {
    lateinit var binding: ActivityClientBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val multicastModel = ViewModelProviders.of(this, MulticastViewModelFactory(this)).get(MultiCastViewModel::class.java)


        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_client
        )

        val model = ViewModelProviders.of(this,
            ClientViewModelFactory(this)
        ).get(ClientViewModel::class.java)

        binding.connectButton.setOnClickListener { view -> model.connect(binding.serverIpEditText.text.toString(),
            ServerSocket.PORT
        )}
        binding.sendButton.setOnClickListener { view -> model.send(binding.messageEditText.text.toString()) }

        val adapter = MessageAdapter {}
        binding.messageList.adapter = adapter

        /*model.messages.observe(this, Observer { list ->
            adapter.list = list
        })*/

        multicastModel.messages.observe(this, Observer { list ->
            adapter.list = list
        })

        model.connected.observe(this, Observer { bool->
            binding.sendButton.setEnabled(bool)
        })
    }
 }
